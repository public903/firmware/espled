# LED Controller
## Manuál
######  jatel.sk

### Parametre:
**Napájanie:**
Napätie Un: 230V 50Hz
Prúd In: 1A
**Výstup:**
GPIO2 - 12V PWM 190W

### Popis:
El. zariadenie je programovatelný PWM ovládač osvetlenia pomocou 12V LED svietidiel o max výkone 190W. Obsahuje WiFi komunikáciu ktorá slúži na synchronizáciu času protokolom NTP a konfiguráciu resp programovanie svetelných scén.

### Vlastnosti:
- ovládanie osvetlenia s PWM reguláciou
- nastavenie hodiny a minúty zopnutia, výkonu a doby stmievania
- jednorazový dej s troma zmenami
- NTP server - synchronizácia času cez internet
- WiFi pripojenie
- WEB konfigurácia

### Prvé nastavenie:
Kontrolér je potrebné pripojiť na sieť WiFi ktorá má prístup na internet, pretože využíva na synchronizáciu času NTP server. Na to aby sa dokázal pripojiť na WiFi sieť je potrebné zadať názov siete SSID a heslo. To je možné urobiť pomocou ďalšieho zariadenia ako je napr. smartfón, PC alebo tablet. Po zapnutí sa kontrolér snaží automaticky pripojiť na WiFi sieť. Ak sa mu to však nepodarí, vytvorí vlastnú sieť, tzv AP - Access point s názvom „ESPLED“. Na túto sieť sa pripojíme druhým zariadením, napr. smartfónom. Prístup nieje zaheslovaný. Po pripojení sa nám ukáže konfiguračné menu, kde zvolíme Network configuration, nastavíme názov siete a heslo. Stlačíme tlačidlo SAVE a po tomto úkone sa zariadenie pokúsi pripojiť k zadanej sieti. Ak je pripojenie úspešné, AP zanikne a odteraz už bude možné s kontrolérom komunikovať iba v danej sieti.
