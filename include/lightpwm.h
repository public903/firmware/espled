/* 
 * Library for PWM LED controll
 * 
 * 
 * 
 * Author: Trnik 7.1.2021
 * 
*/

#ifndef LIGHTPWM_H
#define LIGHTPWM_H

#include <Arduino.h>

#define LEDPWMpin 2

class PWMLED {
public:
    PWMLED();
    void begin(uint8_t pinLED);
    void run(void);
    void start(uint8_t light, uint32_t fade);   // light[0-100] - vykon do ktoreho sa ma LEDka dostat za cas fade[s][0-99999]
    void stop(void);
    uint8_t getLight(void);
    uint32_t getFade(void);

private:
    uint32_t millisInit;
    uint8_t pinLED;
    uint16_t lightEnd, fade_counter;
    float fade_step, pwr_step, lightNow;
};

#endif