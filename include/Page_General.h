//
//  HTML PAGE
//

const char PAGE_AdminGeneralSettings[] PROGMEM =  R"=====(
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<a href="admin.html"  class="btn btn--s"><</a>&nbsp;&nbsp;<strong>General Settings</strong>
<hr>
<form action="" method="post">

<h1> LED Controller </h1>
<h2> WEB Configurator </h2>
<h6> jatel.sk </h6>
<h3> Parametre: </h3>
<h4> Napájanie: </h4>
<p> Napätie Un: 230V 50Hz </p>
<p> Prúd In: 1A </p>
<h4> Výstup: </h4>
<p> 12V PWM 160W </p>
<h3> Vlastnosti </h3>
<p> - ovládanie osvetlenia s PWM reguláciou</p>
<p> - nastavenie hodiny a minúty zopnutia, výkonu a doby stmievania</p>
<p> - jednorazový dej s troma zmenami</p>
<p> - NTP server - synchronizácia času cez internet</p>
<p> - WiFi pripojenie</p>
<p> - WEB konfigurácia </p>

<table border="0"  cellspacing="0" cellpadding="3" >
<tr>
	<td align="right">Name of Device</td>
	<td><input type="text" id="devicename" name="devicename" value=""></td>
</tr>

<tr>
	<td align="left" colspan="2"><hr></td>
</tr>
<tr>
	<td align="left" colspan="2">Turn on 1</td>
</tr>
<tr>
	<td align="right"> Time:</td>
	<td><input type="text" id="ton1hour" name="ton1hour" size="2" value="00">:<input type="text" id="ton1minute" name="ton1minute" size="2" value="00"></td>
</tr>
<tr>
	<td align="right"> Power[0-100] : Fade[s[0-99999]]:</td>
	<td><input type="text" id="ton1light" name="ton1light" size="3" value="00">:<input type="text" id="ton1fade" name="ton1fade" size="5" value="00"></td>
</tr>

<tr>
	<td align="left" colspan="2"><hr></td>
</tr>
<tr>
	<td align="left" colspan="2">Turn on 2</td>
</tr>
<tr>
	<td align="right"> Time:</td>
	<td><input type="text" id="ton2hour" name="ton2hour" size="2" value="00">:<input type="text" id="ton2minute" name="ton2minute" size="2" value="00"></td>
</tr>
<tr>
	<td align="right"> Power[0-100] : Fade[s[0-99999]]:</td>
	<td><input type="text" id="ton2light" name="ton2light" size="3" value="00">:<input type="text" id="ton2fade" name="ton2fade" size="5" value="00"></td>
</tr>

<tr>
	<td align="left" colspan="2"><hr></td>
</tr>
<tr>
	<td align="left" colspan="2">Turn on 3</td>
</tr>
<tr>
	<td align="right"> Time:</td>
	<td><input type="text" id="ton3hour" name="ton3hour" size="2" value="00">:<input type="text" id="ton3minute" name="ton3minute" size="2" value="00"></td>
</tr>
<tr>
	<td align="right"> Power[0-100] : Fade[s[0-99999]]:</td>
	<td><input type="text" id="ton3light" name="ton3light" size="3" value="00">:<input type="text" id="ton3fade" name="ton3fade" size="5" value="00"></td>
</tr>

<tr><td colspan="2" align="center"><input type="submit" style="width:150px" class="btn btn--m btn--blue" value="Save"></td></tr>
</table>
</form>
<script>

 

window.onload = function ()
{
	load("style.css","css", function() 
	{
		load("microajax.js","js", function() 
		{
				setValues("/admin/generalvalues");
		});
	});
}
function load(e,t,n){if("js"==t){var a=document.createElement("script");a.src=e,a.type="text/javascript",a.async=!1,a.onload=function(){n()},document.getElementsByTagName("head")[0].appendChild(a)}else if("css"==t){var a=document.createElement("link");a.href=e,a.rel="stylesheet",a.type="text/css",a.async=!1,a.onload=function(){n()},document.getElementsByTagName("head")[0].appendChild(a)}}



</script>
)=====";


// Functions for this Page
void send_devicename_value_html()
{
		
	String values ="";
	values += "devicename|" + (String) config.DeviceName + "|div\n";
	server.send ( 200, "text/plain", values);
	Serial.println(__FUNCTION__); 
	
}

void send_general_html()
{
	
	if (server.args() > 0 )  // Save Settings
	{
		String temp = "";
		for ( uint8_t i = 0; i < server.args(); i++ ) {
			if (server.argName(i) == "devicename") config.DeviceName = urldecode(server.arg(i)); 
			if (server.argName(i) == "ton1hour") config.TurnOn1Hour = server.arg(i).toInt();
			if (server.argName(i) == "ton1minute") config.TurnOn1Minute = server.arg(i).toInt();
			if (server.argName(i) == "ton1light") config.TurnOn1Light =  server.arg(i).toInt();
			if (server.argName(i) == "ton1fade") config.TurnOn1Fade =  server.arg(i).toInt();
			if (server.argName(i) == "ton2hour") config.TurnOn2Hour = server.arg(i).toInt();
			if (server.argName(i) == "ton2minute") config.TurnOn2Minute = server.arg(i).toInt();
			if (server.argName(i) == "ton2light") config.TurnOn2Light =  server.arg(i).toInt();
			if (server.argName(i) == "ton2fade") config.TurnOn2Fade =  server.arg(i).toInt();
			if (server.argName(i) == "ton3hour") config.TurnOn3Hour = server.arg(i).toInt();
			if (server.argName(i) == "ton3minute") config.TurnOn3Minute = server.arg(i).toInt();
			if (server.argName(i) == "ton3light") config.TurnOn3Light =  server.arg(i).toInt();
			if (server.argName(i) == "ton3fade") config.TurnOn3Fade =  server.arg(i).toInt();
		}
		WriteConfig();
		firstStart = true;
	}
	server.send ( 200, "text/html", PAGE_AdminGeneralSettings ); 
	Serial.println(__FUNCTION__); 
	
	
}

void send_general_configuration_values_html()
{
	String values ="";
	values += "devicename|" +  (String)  config.DeviceName +  "|input\n";
	values += "ton1hour|" +  (String)  config.TurnOn1Hour +  "|input\n";
	values += "ton1minute|" +   (String) config.TurnOn1Minute +  "|input\n";
	values += "ton1light|" +  (String)  config.TurnOn1Light +  "|input\n";
	values += "ton1fade|" +   (String)  config.TurnOn1Fade +  "|input\n";
	values += "ton2hour|" +  (String)  config.TurnOn2Hour +  "|input\n";
	values += "ton2minute|" +   (String) config.TurnOn2Minute +  "|input\n";
	values += "ton2light|" +  (String)  config.TurnOn2Light +  "|input\n";
	values += "ton2fade|" +   (String)  config.TurnOn2Fade +  "|input\n";
	values += "ton3hour|" +  (String)  config.TurnOn3Hour +  "|input\n";
	values += "ton3minute|" +   (String) config.TurnOn3Minute +  "|input\n";
	values += "ton3light|" +  (String)  config.TurnOn3Light +  "|input\n";
	values += "ton3fade|" +   (String)  config.TurnOn3Fade +  "|input\n";
	server.send ( 200, "text/plain", values);
	Serial.println(__FUNCTION__); 
}