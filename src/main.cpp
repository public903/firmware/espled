/* 
  ESP_WebConfig 

  Copyright (c) 2015 John Lassen. All rights reserved.
  This is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  This software is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  Latest version: 1.1.3  - 2015-07-20
  Changed the loading of the Javascript and CCS Files, so that they will successively loaded and that only one request goes to the ESP.

  -----------------------------------------------------------------------------------------------
  History

  Version: 1.1.2  - 2015-07-17
  Added URLDECODE for some input-fields (SSID, PASSWORD...)

  Version  1.1.1 - 2015-07-12
  First initial version to the public
  
*/

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <Ticker.h>
#include <EEPROM.h>
#include <WiFiUdp.h>
#include "helpers.h"
#include "global.h"
#include <DNSServer.h>
#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager
#include "lightpwm.h"
#include <ESP8266mDNS.h>
/*
Include the HTML, STYLE and Script "Pages"
*/
#include "Page_Root.h"
#include "Page_Admin.h"
#include "Page_Script.js.h"
#include "Page_Style.css.h"
#include "Page_NTPSettings.h"
#include "Page_Information.h"
#include "Page_General.h"
#include "PAGE_NetworkConfiguration.h"
/*
**  Objects
*/
PWMLED Light;


void setup ( void ) {
	Light.begin(LEDPWMpin);
	EEPROM.begin(512);
	Serial.begin(115200);
	delay(100);
	Serial.println("Starting ESP8266");
	
  	WiFiManager wifiManager;
  	//reset settings - for testing
  	//wifiManager.resetSettings();
  	wifiManager.setTimeout(300);
  	if(!wifiManager.autoConnect("ESPLED")) {
    	Serial.println("failed to connect and hit timeout");
    	delay(3000);
    	//reset and try again, or maybe put it to deep sleep
    	ESP.reset();
    	delay(5000);
  	} 
  	//if you get here you have connected to the WiFi
  	Serial.println("connected...");

	if (!ReadConfig())
	{
		// DEFAULT CONFIG
		config.ssid = "ssid";	// nepoužíva sa - rieši to <WiFiManager.h>
		config.password = "password";	// nepoužíva sa - rieši to <WiFiManager.h>
		config.dhcp = true;
		config.IP[0] = 192;config.IP[1] = 168;config.IP[2] = 1;config.IP[3] = 100;
		config.Netmask[0] = 255;config.Netmask[1] = 255;config.Netmask[2] = 255;config.Netmask[3] = 0;
		config.Gateway[0] = 192;config.Gateway[1] = 168;config.Gateway[2] = 1;config.Gateway[3] = 1;
		config.ntpServerName = "0.de.pool.ntp.org";
		config.Update_Time_Via_NTP_Every =  60;
		config.timezone = 1;
		config.daylight = true;
		config.DeviceName = "LED Controller";
		config.TurnOn1Hour = 10;
		config.TurnOn1Minute = 0;
		config.TurnOn1Light = 20;
		config.TurnOn1Fade = 60;
		config.TurnOn2Hour = 16;
		config.TurnOn2Minute = 0;
		config.TurnOn2Light = 80;
		config.TurnOn2Fade = 3600;
		config.TurnOn3Hour = 22;
		config.TurnOn3Minute = 0;
		config.TurnOn3Light = 0;
		config.TurnOn3Fade = 60;
		WriteConfig();
		Serial.println("General config applied");
	}
	
	// Set up mDNS responder:
    if (!MDNS.begin("espled")) {
      Serial.println("Error setting up MDNS responder!");
      while (1) {
        delay(1000);
      }
    }
    Serial.println("mDNS responder started");

	server.on ( "/", []() { Serial.println("admin.html"); server.send ( 200, "text/html", PAGE_AdminMainPage );   }  );
	server.on ( "/favicon.ico",   []() { Serial.println("favicon.ico"); server.send ( 200, "text/html", "" );   }  );
	server.on ( "/admin.html", []() { Serial.println("admin.html"); server.send ( 200, "text/html", PAGE_AdminMainPage );   }  );
	server.on ( "/info.html", []() { Serial.println("info.html"); server.send ( 200, "text/html", PAGE_Information );   }  );
	server.on ( "/ntp.html", send_NTP_configuration_html  );
	server.on ( "/general.html", send_general_html  );
	server.on ( "/style.css", []() { Serial.println("style.css"); server.send ( 200, "text/plain", PAGE_Style_css );  } );
	server.on ( "/microajax.js", []() { Serial.println("microajax.js"); server.send ( 200, "text/plain", PAGE_microajax_js );  } );
	server.on ( "/admin/connectionstate", send_connection_state_values_html );
	server.on ( "/admin/infovalues", send_information_values_html );
	server.on ( "/admin/ntpvalues", send_NTP_configuration_values_html );
	server.on ( "/admin/generalvalues", send_general_configuration_values_html);
	server.on ( "/admin/devicename",     send_devicename_value_html);

	server.onNotFound ( []() { Serial.println("Page Not Found"); server.send ( 400, "text/html", "Page not Found" );   }  );
	server.begin();
	Serial.println( "HTTP server started" );

	tkSecond.attach(1,Second_Tick);
	UDPNTPClient.begin(2390);  // Port for NTP receive
}

 

void loop ( void ) {
  	MDNS.update();

	if (config.Update_Time_Via_NTP_Every  > 0 )
	{
		if (cNTP_Update > 5 && firstStart)
		{
			NTPRefresh();
			cNTP_Update =0;
			firstStart = false;
		}
		else if ( cNTP_Update > (config.Update_Time_Via_NTP_Every * 60) )
		{

			NTPRefresh();
			cNTP_Update =0;
		}
	}

	if(DateTime.minute != Minute_Old)
	{
		Minute_Old = DateTime.minute;
		if ((config.TurnOn1Hour == DateTime.hour) && (config.TurnOn1Minute == DateTime.minute)) Light.start(config.TurnOn1Light, config.TurnOn1Fade);
		if ((config.TurnOn2Hour == DateTime.hour) && (config.TurnOn2Minute == DateTime.minute)) Light.start(config.TurnOn2Light, config.TurnOn2Fade);
		if ((config.TurnOn3Hour == DateTime.hour) && (config.TurnOn3Minute == DateTime.minute)) Light.start(config.TurnOn3Light, config.TurnOn3Fade);
	}
	server.handleClient();

	/*   Your Code here    */
	Light.run();

	if (Refresh)  
	{
		Refresh = false;
		//Serial.println("Refreshing...");
		//Serial.printf("FreeMem:%d %d:%d:%d %d.%d.%d \n",ESP.getFreeHeap() , DateTime.hour,DateTime.minute, DateTime.second, DateTime.year, DateTime.month, DateTime.day);
	}
}

