#include <Arduino.h>
#include "lightpwm.h"
#include <cmath>

//#define DEBUG

//-------------------------------------------------------------
PWMLED::PWMLED() {
}
//-------------------------------------------------------------
void PWMLED::begin(uint8_t plPinLED) {
    pinLED = plPinLED;
    lightNow = 0;
    pinMode(pinLED, OUTPUT);
    analogWrite(pinLED, lightNow);
    lightEnd = 0;
    fade_step = 0;
    fade_counter = 0;
}
//-------------------------------------------------------------
void PWMLED::run(void) {
  if (!fade_step) return;
  else if ((millis() - millisInit) >= fade_step * (fade_counter+1)) {
    fade_counter++;
    lightNow += pwr_step;
    if ((pwr_step > 0) && (lightNow >= lightEnd)) {
      lightNow = lightEnd;
      fade_step = 0;
    } else if ((pwr_step < 0) && (lightNow < lightEnd)) {
      lightNow = lightEnd;
      fade_step = 0;
    }
    #ifdef DEBUG
      Serial.printf("lightNow=%f; fcounter=%d\n",lightNow,fade_counter);
    #endif
    analogWrite(pinLED, lightNow);
  }
}
//-------------------------------------------------------------
void PWMLED::start(uint8_t light, uint32_t fade) {   // light[0-100] - vykon do ktoreho sa ma LEDka dostat za cas fade[s][0-99999]
  millisInit = millis();
  fade_counter = 0;
  lightEnd = 10.23 * light;
  int16_t pwr_interval = lightEnd - lightNow;
  if (pwr_interval == 0) return;
  fade_step = (((float)fade * 1000) / abs(pwr_interval));
  if (fade_step == 0) {   // zmena bez stmievania
    lightNow = lightEnd;
    analogWrite(pinLED, lightNow);
  }
  else if (fade_step >= 50) {   // casovy krok stmievania je 50ms alebo viac
    if (pwr_interval < 0) pwr_step = -1;
    else pwr_step = 1;
  } else if(fade_step < 50) {   // casovy krok stmievania je menej ako 50ms
    fade_step = 50;
    pwr_step = pwr_interval / ((fade * 1000) / fade_step);   // prerataj pwm kroky tak aby casovy krok bol 50ms
  }
  #ifdef DEBUG
    Serial.print(" lightNow = ");Serial.print(lightNow);Serial.print(" lightEnd = ");Serial.print(lightEnd);
    Serial.print(" pwr interval = ");Serial.print(pwr_interval);Serial.print(" fade step = ");Serial.print(fade_step);
    Serial.print(" pwr_step = ");Serial.print(pwr_step);Serial.println();Serial.println();
  #endif
}
//-------------------------------------------------------------
void PWMLED::stop(void) {
  fade_step = 0;
  lightNow = 0;
  lightEnd = 0;
  analogWrite(pinLED, lightNow);
}
//-------------------------------------------------------------
uint8_t PWMLED::getLight(void) {
  return(lightNow);
}
//-------------------------------------------------------------
